package com.example.attendance.controller;

import com.example.attendance.model.Data;
import com.example.attendance.repository.DataRepository;
import com.example.attendance.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:8081")
@RequestMapping("/api")
public class DataController {
    @Autowired
    DataService dataService;

    @Autowired
    DataRepository dataRepository;

    @RequestMapping(value = "/absen", method = RequestMethod.GET)
    public ResponseEntity<?> getAll(@Param("name") String name, @Param("date") String date) {
        List<Data> data = dataService.getAllAttendance(name, date);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

//    -----------------------------AWAL PAGINATION----------------------------------------------

//    @RequestMapping(value = "/absen", method = RequestMethod.GET)
//    public ResponseEntity<Map<String, Object>> getAllattendancePage(
//            @RequestParam(required = false) String attendanceUser,
//            @RequestParam(defaultValue = "0") int page,
//            @RequestParam(defaultValue = "10") int size,
//            @RequestParam(defaultValue = "id,desc") String[] sort) {
//
//        try {
//            List<Order> orders = new ArrayList<Order>();
//
//            if (sort[0].contains(",")) {
//                // will sort more than 2 fields
//                // sortOrder="field, direction"
//                for (String sortOrder : sort) {
//                    String[] _sort = sortOrder.split(",");
//                    orders.add(new Order(dataService.getSortDirection(_sort[1]), _sort[0]));
//                }
//            } else {
//                // sort=[field, direction]
//                orders.add(new Order(dataService.getSortDirection(sort[1]), sort[0]));
//            }
//
//            List<Data> datas = new ArrayList<Data>();
//            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));
//
//            Page<Data> pageTuts;
//            if (attendanceUser == null)
//                pageTuts = dataRepository.findAll(pagingSort);
//            else
//                pageTuts = dataRepository.findByAttendanceUserContaining(attendanceUser, pagingSort);
//
//            datas = pageTuts.getContent();
//
//            Map<String, Object> response = new HashMap<>();
//            response.put("data", datas);
//            response.put("currentPage", pageTuts.getNumber());
//            response.put("totalItems", pageTuts.getTotalElements());
//            response.put("totalPages", pageTuts.getTotalPages());
//
//            return new ResponseEntity<>(response, HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

//    ----------------------------------AKHIR PAGINATION----------------------------------------

    @RequestMapping(value = "/absen/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        Optional<Data> data = dataService.getAttendanceById(id);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/absen", method = RequestMethod.POST)
    public ResponseEntity<?> addAttendance(@RequestBody Data data) {
        Data dataa = dataService.addAttendance(data);
        return new ResponseEntity<>(dataa, HttpStatus.OK);
    }

    @RequestMapping(value = "/absen/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateAttendance(@PathVariable("id") Long id, @RequestBody Data data) {
        Data dataa = dataService.updateAttendance(id, data.getAttendanceUser());
        return new ResponseEntity<>(dataa, HttpStatus.OK);
    }

    @RequestMapping(value = "/absen/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteAttendance(@PathVariable("id") Long id) {
        dataService.deleteDataAttendance(id);
        return new ResponseEntity<>("Delete success! ", HttpStatus.OK);
    }

    @RequestMapping(value = "/absen/download", method = RequestMethod.GET)
    public ResponseEntity<Resource> getFile() {
        String filename = "attdance-list.xlsx";
        InputStreamResource file = new InputStreamResource(dataService.download());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }
}