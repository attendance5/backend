package com.example.attendance.repository;

import com.example.attendance.model.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DataRepository extends JpaRepository<Data, Long> {

//    -----------------------AWAL PAGINATION----------------------------------------------

//    List<Data> findByAttendanceUserContaining(String attendanceUser, Sort sort);
//    Page<Data> findByAttendanceUserContaining(String attendanceUser, Pageable pageable);

//    ----------------------------AKHIR PAGINATION----------------------------------------

    @Query("SELECT p FROM Data p WHERE CONCAT(p.attendanceUser) LIKE %?1%")
    List<Data> searchByName(String name);

    @Query("SELECT p FROM Data p WHERE CONCAT(p.attendanceDate) LIKE %?1%")
    List<Data> searchByDate(String date);
}