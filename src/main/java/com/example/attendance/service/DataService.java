package com.example.attendance.service;

import com.example.attendance.helper.ExcelHelper;
import com.example.attendance.model.Data;
import com.example.attendance.repository.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Optional;

@Service
public class DataService {
    @Autowired
    DataRepository dataRepository;

    public List<Data> getAllAttendance(String name, String date) {
        if (name != null) {
            return dataRepository.searchByName(name);
        } else if (date != null) {
            return dataRepository.searchByDate(date);
        } else {
            return dataRepository.findAll();
        }
    }

//    ----------------------------AWAL PAGINATION----------------------------------------

//    public Sort.Direction getSortDirection(String direction) {
//        if (direction.equals("asc")) {
//            return Sort.Direction.ASC;
//        } else if (direction.equals("desc")) {
//            return Sort.Direction.DESC;
//        }
//        return Sort.Direction.ASC;
//    }

//    ----------------------------AKHIR PAGINATION----------------------------------------
    
    public Optional<Data> getAttendanceById(Long id) {
        return dataRepository.findById(id);
    }

    public Data addAttendance(Data data) {
        return dataRepository.save(data);
    }

    public Data updateAttendance(Long id, String attendanceUser) {
        Data data = dataRepository.findById(id).orElse(null);
        data.setAttendanceUser(attendanceUser);
        return dataRepository.save(data);
    }

    public void deleteDataAttendance(Long id) {
        dataRepository.deleteById(id);
    }

    public ByteArrayInputStream download() {
        List<Data> data = dataRepository.findAll();
        ByteArrayInputStream in = ExcelHelper.DataToExcel(data);
        return in;
    }
}